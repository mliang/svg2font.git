### 特别说明

做这个工具是受https://github.com/MichealWayne/svgs2fonts的启发。介绍文档：http://blog.michealwayne.cn/2018/07/26/notes/%E3%80%90%E7%AC%94%E8%AE%B0%E3%80%91%E7%94%B1iconfont%E5%BC%95%E8%B5%B7%E7%9A%84svg%E3%80%81ttf%E3%80%81woff%E3%80%81woff2%E5%9B%BE%E6%A0%87%E7%9A%84%E7%A0%94%E7%A9%B6%E5%8F%8A%E5%85%B6%E8%BD%AC%E6%8D%A2/


最开始是想找一个可以直接用的工具来生成fonts图标，当时找到svgs2fonts后感觉基本上也解决了问题，但是这个工具可以生成demo.html，但是项目中直接用却需要将demo.hmtl中的css拷贝出来，虽然很好，但还可以更好，查看源码之后发现并没有生成css的方法，当然也可以fork后二次开发，但是基于对github稳定性的不信任，还是决定在gitos上重新实现。

整体上的思路完全来自svgs2fonts, 但是具体代码实现完全是自主开发的，转换的代码来自各个依赖工具本身提供的代码。

生成css文件使用了less，这是一个很坑的东西，出错没有代码提示，基本就是在试错。内置函数各种问题，关键是外部传参，简直欲仙欲死。

生成demo.html使用了ejs，这个就比较友好了，没出什么幺蛾子。

### 依赖

svgicons2svgfont： 将多个svg文件合并成一个svgfont文件

svg2ttf： 将svgfont文件转换为ttf文件

ttf2eot： 将ttf文件转换为eot文件

ttf2woff： 将ttf文件转换为woff文件

ttf2woff2： 将ttf文件转换为woff2文件

less: 生成svgfont.css

ejs: 生成svgdemo.html

### 使用

工具已经上传npm，搜索'svg2font-cmss'，就可以找到，使用1.0.2版本，前两个版本是测试的。

```
const svg2font = require('svg2font-cmss');


svg2font.build(svgPath,fontPath);
```